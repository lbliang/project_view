// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/index.js'
import 'jquery'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'module/bootstrap/dist/css/bootstrap.min.css'
import 'module/bootstrap/dist/js/bootstrap.min.js'
import 'module/swiper/dist/css/swiper.css'
import './axios/'
import Axios from 'axios'
Vue.prototype.$http = Axios

Vue.config.productionTip = false
Vue.use(VueAwesomeSwiper)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
