import axios from 'axios'
import store from '../store/'
import router from '../router'

axios.interceptors.request.use(
  config => {
    if (store.state.user.token) {
      config.headers.Authorization = `JWT ${store.state.user.token}`
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)

axios.interceptors.response.use(
  undefined,
  error => {
    let res = error.response
    if (res.status === 401) {
      router.replace({
        path: '/login',
        query: {redirect: router.currentRoute.fullPath}
      })
    }
    return Promise.reject(error.response.data)
  }
)
