import {getTestPaper} from '@/api/api'
import {containClassified} from './utils.js'
import store from '../store/'
import router from '../router'

export const setTestPaper = data => {
  store.state.testpaper.id = data.id
  store.state.testpaper.difficulty = data.difficulty[0].toUpperCase + data.difficulty.substr(1)
  store.state.testpaper.level = data.level
  store.state.testpaper.problem_num = data.problem_num
  let record = data.learning_style.split('_')
  store.state.testpaper.learning_style = ''
  for (let i = 0; i < record.length; ++i) {
    if (store.state.testpaper.learning_style) {
      store.state.testpaper.learning_style += ' '
    }
    store.state.testpaper.learning_style += record[i][0].toUpperCase() + record[i].substr(1)
  }
  store.state.testpaper.contain_classified = containClassified(data.contain_classified)
}

const requestTestPaper = params => {
  getTestPaper({id: params.id}).then(response => {
    const data = response.data
    setTestPaper(data)
    router.replace({
      path: '/Examination',
      query: {redirect: router.currentRoute.fullPath}
    })
  })
}

export default requestTestPaper
