import axios from 'axios'

let host = 'http://123.207.58.66:8000'

// 获取首页中间块介绍信息
export const getHomeBlocks = () => { return axios.get(`${host}/homeblocks/`) }

// 获取轮播图列表
export const getSwiperPhotos = () => { return axios.get(`${host}/swiperphotos/`) }

// 登录
export const login = params => { return axios.post(`${host}/login/`, params) }

// 注册
export const register = params => { return axios.post(`${host}/users/`, params) }

// 获取层次选择列表
export const getLevelList = () => { return axios.get(`${host}/levellist/`) }

// 获取学习方式列表
export const getLearningStyles = () => { return axios.get(`${host}/learningstyles/`) }

// 获取对应层次的题目类型列表
export const getTypeList = params => { return axios.get(`${host}/typelist/`, {params}) }

// 提交选择信息生成对应套卷
export const postTestPaper = params => { return axios.post(`${host}/testpaper/`, params) }

// 获取对应的套卷
export const getTestPaper = params => { return axios.get(`${host}/testpaper/` + params.id + '/') }

// 获取对应的套卷列表
export const getTestPaperList = params => { return axios.get(`${host}/testpaper/`, {params}) }

// 获取对应的题目
export const getTestProblem = params => { return axios.get(`${host}/testproblem/`, {params}) }

// 获取选择题的对应选项
export const getSelectInfo = params => { return axios.get(`${host}/selectinfo/`, {params}) }

// 更新出题题目的信息
export const postTestProblem = params => { return axios.patch(`${host}/testproblem/${params.id}/`, params) }

// 获取用户列表
export const getUsers = params => { return axios.get(`${host}/users/`, {params}) }

// 提交试卷
export const submitTestPaper = params => { return axios.patch(`${host}/testpaper/${params.id}/`, params) }

// 获取用户题目联系信息
export const getUserProblem = params => { return axios.get(`${host}/userproblem/`, {params}) }

// 更新用户信息
export const updateUserInfo = params => { return axios.patch(`${host}/users/${params.id}/`, params) }

// 获取用户密保问题
export const getSecurityProblems = params => { return axios.get(`${host}/securityproblems/`, {params}) }
