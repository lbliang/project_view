<template>
<div class="modal fade" id="achievement">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" @click="handleClose"><span aria-hidden="true">&times;</span></button>
        <h1 class="modal-title text-center">
          <i>Historical Paper</i>
        </h1>
      </div>
      <div class="col-md-push-1 col-md-10 line"></div>
      <div class="modal-body">
        <table class="table">
          <tr>
            <th><i>Keyword</i></th>
            <th><i>Value</i></th>
            <th><i>Description</i></th>
          </tr>
          <tr v-for="(item, key) in paper" :key="key">
            <td class="col-md-4"><i>{{key}}</i></td>
            <td class="col-md-4 text-center">
              <div :class="'middle-value col-md-6 col-md-push-3 ' + middle_classes[item.index-1]">
                <i>{{item.value}}</i>
              </div>
            </td>
            <td class="col-md-4"><i>{{item.description}}</i></td>
          </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button
          type="button"
          class="btn btn-info pull-left"
          data-dismiss="modal"
          @click="handleClose"
        ><i>Close</i></button>
        <button
          type="button"
          class="btn btn-success"
          data-dismiss="modal"
          @click="viewDetail"
        >
          <i>View Detail</i>
        </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</template>

<script>
import Vue from 'vue'
import {calRate, showLevel} from '@/js/utils.js'
import {mapState} from 'vuex'
export default {
  name: 'Achievement',
  props: {
    testpaper: {
      type: Object
    }
  },
  data () {
    return {
      paper: {
        'level': {
          value: 'CET six',
          description: "The paper's level",
          index: 1
        },
        'Paper Score': {
          value: 90,
          description: "Full paper's score: ",
          index: 2
        },
        'Rank Score': {
          value: 66,
          description: "Full rank's score: ",
          index: 3
        },
        'Problem Num': {
          value: 88,
          description: 'Number of Problems',
          index: 4
        },
        'Correct Rate': {
          value: '96.66%',
          description: 'Average correct rate of problem making',
          index: 5
        },
        'Making Integrity': {
          value: '90%',
          description: 'The rate of how many problems have been done',
          index: 6
        },
        'Used Hints': {
          value: 6,
          description: 'The sum of how many hints are used',
          index: 7
        }
      },
      class_index: 0,
      middle_classes: []
    }
  },
  created () {
    for (let i = 0; i < 8; ++i) {
      this.middle_classes.push(this.middleClass())
    }
  },
  methods: {
    middleClass () {
      ++this.class_index
      if ((this.class_index & 1) === 0) {
        return 'even-class'
      } else {
        return 'odd-class'
      }
    },
    handleClose () {
      this.$emit('close')
    },
    viewDetail () {
      this.showTestPaperObj.data = this.testpaper
      this.$router.push('/HistoricalDetail')
    }
  },
  watch: {
    testpaper () {
      const level = this.testpaper.level
      Vue.set(this.paper['level'], 'value', showLevel(level))
      Vue.set(this.paper, 'Paper Score', {
        value: this.testpaper.sum_score,
        description: `Full paper's score: ${this.testpaper.full_score}`,
        index: 2
      })
      Vue.set(this.paper, 'Rank Score', {
        value: this.testpaper.sum_rank_score,
        description: `Full rank's score: ${this.testpaper.full_rank_score}`,
        index: 3
      })
      Vue.set(this.paper['Problem Num'], 'value', this.testpaper.problem_num)
      Vue.set(this.paper['Correct Rate'], 'value', calRate(this.testpaper.cal_correct_rate))
      Vue.set(this.paper['Making Integrity'], 'value', calRate(this.testpaper.cal_integrity))
      Vue.set(this.paper['Used Hints'], 'value', this.testpaper.sum_tips)
    }
  },
  computed: {
    ...mapState(['showTestPaperObj'])
  }
}
</script>

<style lang="stylus" scoped>
  .modal-content
    background: #f3faf4
  .close
    font-size: 6rem
  .modal-header
    border: none
  .line
    border-bottom: 0.2rem solid green
  .table
    th
      text-align: center
      font-weight: 900
      font-size: 2.5rem
      color: #115848
    td
      padding: 0.1rem 0rem
      line-height: 2rem
      height: 4.5rem
      text-align: center
      font-size: 2rem
      font-weight: 700
    .middle-value
      border-radius: 1rem
      line-height: 3rem
      color: #e0f9e4
    .even-class
      background: #80c26a
    .odd-class
      background: #f6b37f
  .btn
    font-size: 2rem
</style>
