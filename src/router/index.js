import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

var router = new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import('@/pages/Home/Home'),
      meta: '首页'
    }, {
      path: '/login',
      name: 'UserLogin',
      component: () => import('@/pages/UsersLoginRegister/UserLogin'),
      meta: '登录'
    }, {
      path: '/register',
      name: 'UserRegister',
      component: () => import('@/pages/UsersLoginRegister/UserRegister'),
      meta: '注册'
    }, {
      path: '/community',
      name: 'Community',
      component: () => import('@/pages/Community/Community'),
      meta: '社区'
    }, {
      path: '/Personal',
      name: 'Personal',
      component: () => import('@/pages/Personal/Personal'),
      meta: '个人中心'
    }, {
      path: '/Study',
      name: 'Study',
      component: () => import('@/pages/Study/Study'),
      meta: '自学室'
    }, {
      path: '/Selection',
      name: 'Selection',
      component: () => import('@/pages/Selection/Selection'),
      meta: '套卷配置选择'
    }, {
      path: '/Examination',
      name: 'Examination',
      component: () => import('@/pages/Examination/Examination'),
      meta: '考试'
    }, {
      path: '/HistoricalDetail',
      name: 'HistoricalDetail',
      component: () => import('@/pages/HistoricalDetail/HistoricalDetail'),
      meta: '做题详情'
    }, {
      path: '/Forget',
      name: 'UserForget',
      component: () => import('@/pages/UsersLoginRegister/UserForget'),
      meta: '忘记密码'
    }
  ]
})

router.afterEach((to, from, next) => {
  document.title = to.matched[to.matched.length - 1].meta
})

export default router
