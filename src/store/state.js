const user = {
  username: null,
  token: ''
}

const selection = {
  level: null,
  problem_num: 0,
  learning_style: null,
  contain_classified: '',
  difficulty: ''
}

const testpaper = {
  id: 0,
  level: 0,
  learning_style: '',
  problem_num: 0,
  difficulty: '',
  contain_classified: ''
}

const showTestPaperObj = {
  data: null
}

const securityProblems = {
  data: null,
  username: '',
  user_id: 0
}

export default {
  user,
  selection,
  testpaper,
  showTestPaperObj,
  securityProblems
}
