// 处理类型显示
export const containClassified = data => {
  let record = data.split(' ')
  let str = ''
  for (let i = 0; i < record.length; ++i) {
    const tmp = record[i].split('_')
    if (str) {
      str += '、'
    }
    for (let j = 0; j < tmp.length; ++j) {
      str += tmp[j][0].toUpperCase() + tmp[j].substr(1) + ' '
    }
  }
  return str
}

// 处理层次显示
export const showLevel = level => {
  const levelName = level.level_name
  if (levelName === 'CET4' || levelName === 'CET6') {
    return levelName.substr(0, 3) + '-' + levelName.substr(3)
  } else {
    return 'PGD'
  }
}

// 处理百分比
export const calRate = rate => {
  return (rate.toFixed(4) * 100).toFixed(2) + '%'
}

// 计算分页显示的分页选项
export const getPageList = (paperList, page, paperCount) => {
  if (paperList.length === 0) {
    return 1
  }
  const count = Math.ceil(paperCount / 6)
  if (count <= 5) {
    return count
  } else {
    const showSum = 5
    const pageList = []
    let l = Math.max(page - 2, 1)
    let r = Math.min(page + 2, count)
    if (page - l < 2) {
      r = Math.min(count, page + (showSum - (page - l + 1)))
    } else if (r - page < 2) {
      l = Math.max(1, page - (showSum - (r - page + 1)))
    }
    for (let i = l; i <= r; ++i) {
      pageList.push(i)
    }
    return pageList
  }
}

// 去掉前后空格
export const trim = data => { return data.replace(/(\s*)|(\s*$)/g, '') }

export const check = (data, pattern) => {
  return pattern.test(data)
}

// 验证用户名（至少4位最多16位)
export const checkUserName = data => {
  return check(data, /^[a-zA-Z0-9_]{4,16}$/)
}

// 验证密码(要有数字和字母)
export const checkPassword = data => {
  const pattern = /^.*(?=.{6,})(?=.*\d)(?=.*[A-Za-z]).*$/
  return check(data, pattern)
}

// 验证邮箱
export const checkEmail = data => {
  const pattern = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/
  return check(data, pattern)
}
